import cv2
import os
import numpy as np
from sklearn.neighbors.kd_tree import KDTree


def build_tree_for_directory(dirname, output, tree=None, threshold=None):
    print('Training: {}'.format(dirname))
    surf = cv2.xfeatures2d.SURF_create(5000)
    files = os.listdir(dirname)
    selected_desc = []

    for f in files:
        input_path = os.path.join(dirname, f)
        im = cv2.imread(input_path)
        kp, descriptors = surf.detectAndCompute(im, None)
        selected_kp = []
        if tree is not None:
            dists = tree.query(descriptors)
            for i in range(len(kp)):
                if dists[0][i, 0] < threshold:
                    selected_kp.append(kp[i])
                    selected_desc.append(descriptors[i])
        else:
            selected_kp.extend(kp)
            selected_desc.extend(descriptors)

        im = cv2.drawKeypoints(im, selected_kp, None, (255, 0, 0), 4)
        print("{} {} {}".format(input_path, len(kp), len(selected_kp)))
        cv2.imwrite(os.path.join(output, f), im)

    np.save('last_desc', np.array(selected_desc))

    return KDTree(selected_desc)


train = True

if train:
    initial_tree = build_tree_for_directory('InitialSet', 'InitialValidation')
    actual_tree = build_tree_for_directory('TrainSet', 'TrainValidation', initial_tree, 0.18)
else:
    actual_tree = KDTree(np.load('last_desc.npy'))


def find_patterns_image(image, keypoints):
    def distance(a, b):
        a = np.array(a.pt)
        b = np.array(b.pt)
        return np.linalg.norm(b - a, ord=2)

    def near_saved(saved, kp):
        x, y, r = saved
        vect = np.array([x, y]) - [kp[0], kp[1]]
        return np.linalg.norm(vect, 2) < r

    saved = []

    def add_point(x, y, size):
        for p in saved:
            if near_saved(p, (x, y)):
                return
        cv2.circle(image, (int(x), int(y)), int(size), (0, 255, 0), 2)
        saved.append((x, y, size))

    sizes = [kp.size for kp in keypoints]
    keypoints_big = [kp for kp in keypoints if kp.size > np.max(sizes) / 1.4]
    keypoints_small = [kp for kp in keypoints if kp.size <= np.max(sizes) / 1.4]
    if len(keypoints_small) < 2:
        keypoints_big = []
        keypoints_small = keypoints

    for kp in keypoints_big:
        add_point(kp.pt[0], kp.pt[1], kp.size / 1.5)
        keypoints_small = [p for p in keypoints_small if distance(kp, p) > kp.size * 2]

    while (len(keypoints_small) > 0):
        kp = keypoints_small[0]
        area = [p for p in keypoints_small if distance(kp, p) < kp.size * 3]

        if len(area) > 1:
            coords = np.array([[p.pt[0], p.pt[1]] for p in area])
            [x, y] = np.mean(coords, axis=0)
            sizes = np.amax(coords, axis=0) - np.amin(coords, axis=0)
            assert len(sizes) == 2
            add_point(x, y, np.mean(sizes))

        for p in area:
            keypoints_small.remove(p)

    return image


def find_patterns_directory(dirname, output, tree=None, threshold=None):
    print('Evaluating: {}'.format(dirname))
    surf = cv2.xfeatures2d.SURF_create(5000)
    files = os.listdir(dirname)

    for f in files:
        input_path = os.path.join(dirname, f)
        im = cv2.imread(input_path)
        kp, descriptors = surf.detectAndCompute(im, None)
        selected_kp = []
        dists = tree.query(descriptors)
        for i in range(len(kp)):
            if dists[0][i, 0] < threshold:
                selected_kp.append(kp[i])

        # im = cv2.drawKeypoints(im, selected_kp, None, (255, 0, 0), 4)
        im = find_patterns_image(im, selected_kp)
        print("{} {} {}".format(input_path, len(kp), len(selected_kp)))
        cv2.imwrite(os.path.join(output, f), im)


find_patterns_directory('TestSet', 'TestOutput', actual_tree, 0.1)
